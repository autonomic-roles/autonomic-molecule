# autonomic-molecule

A base image for speeding up Ansible/Molecule CI runs.

> https://hub.docker.com/r/decentral1se/autonomic-molecule/

**FIXME: Make a specific autonomic dockerhub account**

# Hack On It

```bash
$ make build
```

# Release It

```bash
$ make push
```
