USER  := decentral1se
IMAGE := autonomic-molecule

build:
	@docker build -t $(USER)/$(IMAGE) .
.PHONY: build

login:
	@docker login
.PHONY: login

push:
	@docker push decentral1se/$(IMAGE):latest
.PHONY: push
