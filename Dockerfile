FROM docker:latest

RUN echo "http://dl-3.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

RUN apk add --update gnupg pass py2-pip gcc libffi-dev python-dev musl-dev openssl-dev make linux-headers

RUN pip install -U pip

RUN pip install ansible==2.4.3.0 molecule==2.8.2

RUN pip install "docker<3.0" # https://github.com/ansible/ansible/issues/35612
